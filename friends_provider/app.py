import json, os
from providers.facebook import FacebookParser
from apscheduler.schedulers.background import BackgroundScheduler
from werkzeug.wrappers   import Request, Response
from werkzeug.routing    import Map, Rule
from werkzeug.wsgi       import SharedDataMiddleware, wrap_file
from werkzeug.exceptions import HTTPException, NotFound

data_path   = os.path.join(os.path.dirname(__file__), 'data')
static_path = os.path.join(os.path.dirname(__file__), 'static')
json_path   = os.path.join(data_path, 'friends.json')
stub_path   = os.path.join(data_path, 'stub.json')

class App(object):
    def __init__(self):
        self.url_map = Map([
            Rule('/', endpoint='root'),
            Rule('/friends', endpoint='friends')
        ])

    def on_root(self, environ, request):
        return Response(wrap_file(environ, open(os.path.join(static_path, 'root.html'))), mimetype='text/html')

    def on_friends(self, environ, request):
       if os.path.exists(json_path):
           return Response(wrap_file(environ, open(json_path)), mimetype='application/json')
       else:
           raise NotFound()

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(environ, request)
        return response(environ, start_response)

    def dispatch_request(self, environ, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(environ, request, **values)
        except HTTPException as e:
            return e

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

'''
    syncFriends()
    Runs parsers and merges collected data into JSON file.
'''
def syncFriends():
    if not os.path.exists(data_path):
        os.makedirs(data_path)

    friends = {}
    with open(json_path, 'r') as f:
        for friend in json.load(f).get('friends', []):
            friends[friend['id']] = friend

    friends = {**friends, **FacebookParser()()}
    with open(json_path, 'w') as f:
        json.dump({'friends' : list(friends.values())}, f)

def merge_friends(current, list):
    for friend in l:
        current[friend['id']] = friend
    return current

if __name__ == '__main__':
    '''
        Setup scheduler
    '''
    scheduler = BackgroundScheduler()
    scheduler.add_job(syncFriends, 'interval', seconds=60)
    scheduler.start()

    '''
        Run application
    '''
    from werkzeug.serving import run_simple
    app = App()
    run_simple('0.0.0.0', int(os.getenv('PORT', 3000)), app)
