import time, hashlib, json, os, time
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class FacebookParser(object):

    def __init__(self):
        self.options = Options()
        self.options.add_argument('--headless')
        self.options.add_argument('--log-level=1')

        self.profile = webdriver.FirefoxProfile()
        self.profile.set_preference("dom.webnotifications.enabled", False)

        self.capabilities = webdriver.DesiredCapabilities().FIREFOX

    def append_checksum(self, friend):
        # final parser should take some unique id from fb, cuz this
        # solution will cause conflicts for people with same names
        key = 'facebook_' + str(friend['facebook_id'])
        friend['id'] = hashlib.sha256(key.encode()).hexdigest()
        return friend

    def run_parser(self):
        driver  = webdriver.Firefox(capabilities=self.capabilities, firefox_options=self.options, firefox_profile=self.profile)
        driver.maximize_window()

        print('Connecting to https://www.facebook.com/', flush = True)
        driver.get('https://www.facebook.com/')
        time.sleep(3)

        print("Filling up login form...", flush = True)
        emailInput  = driver.find_element_by_id("email")
        passInput   = driver.find_element_by_id("pass")
        submitInput = driver.find_element_by_id("loginbutton")
        emailInput.send_keys(os.environ['FB_LOGIN'])
        passInput.send_keys(os.environ['FB_PASS'])
        submitInput.submit()
        time.sleep(3)

        driver.get(os.environ['FB_FRIENDS_LIST'])
        time.sleep(5)
        print("Generating JS content...", flush = True)
        for i in range(20): #todo: find a way to prevent hardcoding this
        	driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        	time.sleep(1)

        html = driver.page_source

        print("Shutting off selenium...", flush = True)
        driver.close()

        soup = BeautifulSoup(html, 'lxml')
        results = {}

        for person_block in soup.findAll("div", {"data-testid" : ["friend_list_item"]}):
            person = {}

            name_block     = person_block.find("div", {"class" : ["fsl", "fwb", "fcb"]})
            name_chunks    = name_block.text.split()
            person["name"], person["lastname"] = name_chunks[0], name_chunks[-1]

            img_tag          = person_block.find("img", {"class" : ["img"]})
            person["avatar"] = img_tag.get("src")

            ajax_str              = name_block.find("a").get("data-gt")
            ajax_payload          = json.loads(ajax_str)
            person["facebook_id"] = ajax_payload.get("engagement", {}).get("eng_tid")

            person = self.append_checksum(person)
            results[person["id"]] = person

        return results


    def __call__(self):
        # Friends fetching here
        print('Facebook friends fetching process starting...', flush = True)
        start = time.time()
        data = self.run_parser()
        end = time.time()
        print('Fetching facebook friends completed. Took ' + str(int(end-start)) + ' seconds.', flush = True)
        print("Fetched " + str(len(data)) + " results.", flush = True)
        return data
