import React, { Component } from 'react';

class GradeForm extends Component {

  static defaultProps = {
    marks: [["very_bad", "Very Bad"], ["bad", "Bad"], ["average", "Neutral"], ["good", "Good"], ["very_good", "Very Good"], ["excellent", "Excellent"]],
    defaultGrade: {description: '', mark: 0},
    grade: {description: '', mark: 0}
  }

  constructor(){
    super();
    this.state = {grade: {}};
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentWillMount(){
    this.setState({grade: this.props.grade})
  }

  onFormSubmit(e){
    e.preventDefault();
    // client-side validation here
    this.setState(
      {grade: {description: this.refs.description.value, mark: this.refs.mark.value}},
      () => {this.props.onNewGrade(this.state.grade);}
    );
  }

  render() {
    let options = this.props.marks.map((mark) => {return <option value={mark[0]} key={mark[0]}>{mark[1]}</option>});
    return (
      <div className="GradeForm" onSubmit={this.onFormSubmit}>
        <form>
          <div className="FormField">
            <label>Description: </label>
            <textarea rows="1" cols="30" ref="description" defaultValue={this.state.grade.description}></textarea>
          </div>
          <div className="FormField">
            <label>Mark: </label>
            <select defaultValue={this.props.grade.mark} ref="mark">
              {options}
            </select>
          </div>
          <input type="submit" value={this.props.submitVal}/><br/><br/>
        </form>
      </div>
    );
  }
}

export default GradeForm;
