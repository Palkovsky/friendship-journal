import React, { Component } from 'react';
import GradeForm from './GradeForm'

class Grade extends Component {

  static defaultProps = {
    marks: {"very_bad": 1, "bad": 2, "average": 3, "good": 4, "very_good": 5, "excellent": 6},
    onFocus: () => {return;}
  }

  constructor() {
    super();
    this.onDeleteGrade = this.onDeleteGrade.bind(this);
    this.onUpdateGrade = this.onUpdateGrade.bind(this);
    this.onGradeFocus = this.onGradeFocus.bind(this);
    this.onGradeUnfocus = this.onGradeUnfocus.bind(this);
  }

  onUpdateGrade(updated){
    this.props.onUpdateGrade(this.props.grade, updated);
  }

  onDeleteGrade(e){
    e.preventDefault();
    this.props.onDeleteGrade(this.props.grade);
  }

  onGradeFocus(){
    this.props.onFocus(this.props.grade);
  }

  onGradeUnfocus(){
    this.props.onFocus({grade: -1});
  }

  render() {
    let grade = (
      <div>
        <a className="GradeFormHide" href="#" onClick={this.onGradeUnfocus}>Hide</a>
        <GradeForm grade={this.props.grade} onNewGrade={this.onUpdateGrade} submitVal="Update" />
      </div>);
    if(!this.props.grade.focused){
      grade = (
        <div onClick={this.onGradeFocus}>
          <strong><u>{this.props.marks[this.props.grade.mark]}</u> </strong>{this.props.grade.description}
        </div>
      );
    }
    return (
      <div className="Grade">
        { grade }
        <a href="#" onClick={this.onDeleteGrade}>[X]</a>
      </div>
    );
  }
}

export default Grade;
