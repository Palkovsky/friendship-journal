import React, { Component } from 'react';
import axios from 'axios'
import Grade from './Grade'
import GradeForm from './GradeForm'

class Grades extends Component {

  constructor() {
    super();
    this.state = {
      grades: [],
      newGradeForm: false
    };

    this.fetchRemote = this.fetchRemote.bind(this);
    this.onNewGrade = this.onNewGrade.bind(this);
    this.onUpdateGrade = this.onUpdateGrade.bind(this);
    this.onDeleteGrade = this.onDeleteGrade.bind(this);
    this.unfocus = this.unfocus.bind(this);
    this.onFocusGrade = this.onFocusGrade.bind(this);
    this.showOrHideNewForm = this.showOrHideNewForm.bind(this);
  }

  fetchRemote(){
    axios.get('http://localhost/friends/' + this.props.friend.id + '/grades')
    .then((res) => {
      let newGrades = res.data.payload.map((grade) => {grade.focused = false; return grade;})
      this.setState({grades: newGrades});
    })
    .catch((err) => {
      console.log(err);
    });
  }

  componentWillMount(){
    this.fetchRemote();
  }

  onNewGrade(grade){
    axios.post('http://localhost/friends/' + this.props.friend.id + '/grades', grade)
    .then((res) => {
      let newGrades = this.state.grades;
      newGrades.unshift(res.data.payload);
      this.unfocus();
      this.setState({grades: newGrades, newGradeForm: false});
    })
    .catch((err) => {
      console.log(err);
    });
  }

  onUpdateGrade(grade, updatedFields){
    this.unfocus();
    let updated = {...grade, ...updatedFields};
    axios.put('http://localhost/friends/' + this.props.friend.id + '/grades/' + grade.id, updated)
    .then((res) => {
      let newGrades = this.state.grades;
      newGrades.splice(newGrades.indexOf(grade), 1, updated);
      this.setState({grades: newGrades});
    })
    .catch((err) => {
      console.log(err);
    });
  }

  onDeleteGrade(grade){
    this.unfocus();
    axios.delete('http://localhost/friends/' + this.props.friend.id + '/grades/' + grade.id)
    .then((res) => {
      let newGrades = this.state.grades;
      newGrades.splice(newGrades.indexOf(grade), 1);
      this.setState({grades: newGrades});
    })
    .catch((err) => {
      console.log(err);
    });
  }

  onFocusGrade(grade){
    let newGrades = this.state.grades;
    newGrades.map((g) => {
      g.focused = (grade.id === g.id)
      return g;
    });
    this.setState({grades: newGrades});
  }

  unfocus(){
    this.onFocusGrade({id: -1});
  }

  showOrHideNewForm(e){
    e.preventDefault();
    this.setState({newGradeForm: !this.state.newGradeForm});
  }

  render() {
    let grades = this.state.grades.map(grade => {
      return (<Grade key={grade.id} onFocus={this.onFocusGrade} onUpdateGrade={this.onUpdateGrade} onDeleteGrade={this.onDeleteGrade} grade={grade}/>);
    });
    let gradesWrap = (grades.length === 0) ? (<p>{this.props.friend.name} has no grades</p>) : grades;

    let newGradeForm = (<div><a href="#" onClick={this.showOrHideNewForm}>Hide</a><GradeForm submitVal="Add!" onNewGrade={this.onNewGrade}/></div>)
    if(!this.state.newGradeForm)
      newGradeForm = (<div><a href="#" onClick={this.showOrHideNewForm}>Show form</a></div>)

    return (
      <div className="Grades">
        <div className="GradesListing">
          { gradesWrap }
        </div >
        <p>Add grade for <strong>{this.props.friend.name}</strong> </p>
        { newGradeForm }
        <br/>
      </div>
    )
  }
}

export default Grades;
