import React, { Component } from 'react';
import Friend from './Friend'
import SearchFriend from './SearchFriend'
import axios from 'axios'
import _ from 'lodash'

class Friends extends Component {

  constructor() {
    super();
    this.defAvatar = "https://www.mvhsoracle.com/wp-content/uploads/2018/08/default-avatar.jpg";
    this.allLoadedFriends = [];
    this.state = {
      friends: [],
      page: 1,
      loaded_all: false,
      search: ''
    };

    this.fetchRemote = this.fetchRemote.bind(this);
    this.onUpdateFriend = this.onUpdateFriend.bind(this);
    this.onDeleteFriend = this.onDeleteFriend.bind(this);
    this.onFocusFriend = this.onFocusFriend.bind(this);
    this.onSearchFriend = this.onSearchFriend.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.setFiltered = this.setFiltered.bind(this);
  }

  fetchRemote(){
    axios.get('http://localhost/friends?p=' + this.state.page)
    .then((res) => {
      let friendsList = res.data.payload.map(friend => {
        friend.avatar_url = 'http://localhost/friends/' + friend.id + '/avatar'
        friend.focued = false;
        return friend;
      });
      if(friendsList === undefined || friendsList.length !== 50) this.setState({loaded_all: true});
      this.allLoadedFriends = _.uniq(this.allLoadedFriends.concat(friendsList));
      this.setFiltered();
    })
    .catch((err) => {
      console.log(err);
    });
  }

  componentWillMount(){
    this.setState({page: 1, loaded_all: false});
    this.fetchRemote();
  }

  onUpdateFriend(friend, updatedFields){
    let updated = {...friend, ...updatedFields};
    axios.put('http://localhost/friends/' + updated.id, updated)
    .then((res) => {
        console.log(res);
        let newFriends = this.state.friends;
        newFriends.splice(newFriends.indexOf(friend), 1, updated);
        this.setState({friends: newFriends});
    })
    .catch((err) => {
        console.log(err);
    });
  }

  onDeleteFriend(friend){
    axios.delete('http://localhost/friends/' + friend.id)
    .then((res) => {
        let newFriends = this.state.friends;
        newFriends.splice(newFriends.indexOf(friend), 1);
        this.setState({friends: newFriends});
    })
    .catch((err) => {
        console.log(err);
    });
  }

  onFocusFriend(friend){
    let newFriends = this.state.friends;
    newFriends.map((f) => {
      f.focused = (friend.id === f.id)
      return f;
    });
    this.setState({friends: newFriends});
  }

  onSearchFriend(phrase){
    this.setState({search: phrase}, () => this.setFiltered());
  }

  loadMore(){
    this.setState({page: this.state.page+1}, () => this.fetchRemote());
  }

  setFiltered(){
    let filtered = this.allLoadedFriends.filter((friend) => {
      let nameLastname = (friend.name + friend.lastname).toLowerCase();
      return nameLastname.includes(this.state.search.toLowerCase());
    });
    this.setState({friends: filtered});
  }

  render() {
    let items = this.state.friends.map(friend => {
      return (<Friend key={friend.id} onFocus={this.onFocusFriend} onUpdateFriend={this.onUpdateFriend} onDeleteFriend={this.onDeleteFriend} friend={friend}/>);
    });
    let loadMore = this.state.loaded_all ? null : (<button onClick={this.loadMore}>Load More</button>);
    return (
      <div className="Friends">
        <SearchFriend onKeyPress={this.onSearchFriend}/>
        <div className="FriendsListing">
          <br/>
          { items }
          <br />
        </div >
        { loadMore }
        <p> Loaded total of {this.state.friends.length} friends. </p>
      </div>
    )
  }
}

export default Friends;
