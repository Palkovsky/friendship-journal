import React, { Component } from 'react';
import FriendForm from './FriendForm'
import Grades from '../Grades/Grades'

class Friend extends Component {

  constructor() {
    super();

    this.onDeleteFriend = this.onDeleteFriend.bind(this);
    this.onUpdateFriend = this.onUpdateFriend.bind(this);
    this.onUpdateFriend = this.onUpdateFriend.bind(this);
    this.onFocusFriend = this.onFocusFriend.bind(this);
  }

  onUpdateFriend(updated){
    this.props.onUpdateFriend(this.props.friend, updated);
  }

  onDeleteFriend(e){
    e.preventDefault();
    this.props.onDeleteFriend(this.props.friend);
  }

  onFocusFriend(){
    this.props.onFocus(this.props.friend);
  }

  render() {
    let expanded = null;
    if(this.props.friend.focused){
      expanded = (
        <div className="FriendExpandable">
          <FriendForm friend={this.props.friend} submitVal="Update" onNewFriend={this.onUpdateFriend}/>
          <br/>
          <Grades friend={this.props.friend}/>
        </div>);
    }
    return (
      <div className="Friend" >
        <strong>
          {this.props.friend.name} {this.props.friend.lastname}
        </strong>
        <a href="3" onClick={this.onDeleteFriend}>[X]</a> <br/>
        <div onClick={this.onFocusFriend}>
          <img alt="" src={this.props.friend.avatar_url} /> <br/>
          {this.props.friend.grade_avg} <br/>
          {expanded}
        </div>
      </div>
    );
  }
}

export default Friend;
