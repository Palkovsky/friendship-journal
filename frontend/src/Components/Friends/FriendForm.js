import React, { Component } from 'react';

class FriendForm extends Component {

  static defaultProps = {
    friend: {name: '', lastname: ''}
  }

  constructor(){
    super();
    this.state = {friend: {}};
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentWillMount(){
    this.setState({friend: this.props.friend})
  }

  onFormSubmit(e){
    e.preventDefault();
    // client-side validation here
    this.setState(
      {friend: {name: this.refs.name.value, lastname: this.refs.lastname.value}},
      () => {this.props.onNewFriend(this.state.friend);}
    );
  }

  render() {
    return (
      <div className="FriendForm" onSubmit={this.onFormSubmit}>
        <form>
          <div className="FormField">
            <label>Name: </label>
            <input type="text" ref="name" defaultValue={this.state.friend.name}/>
          </div>
          <div className="FormField">
            <label>Lastname: </label>
            <input type="text" ref="lastname" defaultValue={this.state.friend.lastname}/>
          </div>
          <input type="submit" value={this.props.submitVal}/>
        </form>
      </div>
    );
  }
}

export default FriendForm;
