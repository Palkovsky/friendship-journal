import React, { Component } from 'react';

class SearchFriend extends Component {

  constructor() {
    super();
    this.state = {
      phrase: ''
    }

    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleKeyPress(e){
    this.setState({phrase: e.target.value}, () => this.props.onKeyPress(this.state.phrase));
  }

  render() {
    return (<input type="text" onChange={this.handleKeyPress}/>)
  }
}

export default SearchFriend;
