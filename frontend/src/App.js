import React, { Component } from 'react';
import Friends from './Components/Friends/Friends'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h2>Dzienniki Przyjaźni</h2>
        <Friends />
      </div>
    );
  }
}

export default App;
