{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
module Jobs.CalculateGrade(
    create, spawnWorkers
) where

import Import

import Control.Concurrent
import Control.Monad (replicateM_, forever)
import Helpers.Parsing (parseInt, toInt64)

import Data.ByteString (ByteString)
import qualified Data.Text as T

import Database.Persist.Sql (toSqlKey)
import qualified Database.Redis as R

import Models.GradeValue (gradeValueToNum)

-- BOILERPLATE
create :: App -> ByteString -> IO (Maybe Integer)
create app key = enqueue app key

enqueue :: App -> ByteString -> IO (Maybe Integer)
enqueue app fid = do
    result <- R.runRedis (appRedisPool app) $ R.rpush "CalculateGradeAvarage" [fid]
    return $ case result of
        (Right x) -> Just x
        _         -> Nothing

dequeue :: App -> IO (Maybe ByteString)
dequeue app = do
    result <- R.runRedis (appRedisPool app) $ R.lpop "CalculateGradeAvarage"
    return $ case result of
        Right x -> x
        _       -> Nothing

spawnWorkers :: App -> Int ->  IO ()
spawnWorkers app n = do
    replicateM_ n . forkIO . forever $ do
        mj <- dequeue app
        case mj of
            Just job -> catchAny (perform app job) (\ex -> putStrLn $ (T.pack . show) ex)
            Nothing  -> threadDelay 1000000
-- EOF BOILERPLATE

perform :: App -> ByteString -> IO ()
perform app fid = do
    let friend_id = case parseInt fid of
                    Just i -> (toSqlKey (toInt64 i)) :: FriendId
                    _      -> (toSqlKey 0) :: FriendId
    grades <- runDBIO app $ selectList [GradeFriend ==. friend_id] []
    let (summed, size) = foldr (\(Entity _ grade) (s, c) -> (s + (gradeValueToNum . gradeMark) grade, c+1)) (0, 0) grades
    let avg = if (summed, size) == (0, 0) then 0.0 else (summed :: Double)/size
    runDBIO app $ updateWhere [FriendId ==. friend_id] [FriendGradeAvg =. avg]
