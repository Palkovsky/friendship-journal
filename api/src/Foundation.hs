{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE InstanceSigs #-}

module Foundation where

import Import.NoFoundation
import Database.Persist.Sql (ConnectionPool, runSqlPool)
import Control.Monad.Logger (LogSource)

import Yesod.Core.Types     (Logger)

import System.Directory     (doesFileExist)

import qualified Yesod.Core.Unsafe as Unsafe
import qualified Database.Redis as R

import qualified Magic as M
import qualified Data.Text.Encoding as T
import qualified Data.ByteString as BS

-- | The foundation datatype for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { appSettings    :: AppSettings
    , appConnPool    :: ConnectionPool -- ^ Database connection pool.
    , appHttpManager :: Manager
    , appLogger      :: Logger
    , appRedisPool   :: R.Connection
    }

--
-- This function also generates the following type synonyms:
-- type Handler = HandlerT App IO
-- type Widget = WidgetT App IO ()
mkYesodData "App" $(parseRoutesFile "config/routes")

-- | A convenient synonym for database access functions.
type DB a = forall (m :: * -> *).
    (MonadIO m) => ReaderT SqlBackend m a

instance Yesod App where
    -- Controls the base of generated URLs. For more information on modifying,
    -- see: https://github.com/yesodweb/yesod/wiki/Overriding-approot
    approot :: Approot App
    approot = ApprootRequest $ \app req ->
        case appRoot $ appSettings app of
            Nothing -> getApprootText guessApproot app req
            Just root -> root

    -- Yesod Middleware allows you to run code before and after each handler function.
    -- The defaultYesodMiddleware adds the response header "Vary: Accept, Accept-Language" and performs authorization checks.
    -- Some users may also want to add the defaultCsrfMiddleware, which:
    --   a) Sets a cookie with a CSRF token in it.
    --   b) Validates that incoming write requests include that token in either a header or POST parameter.
    -- To add it, chain it together with the defaultMiddleware: yesodMiddleware = defaultYesodMiddleware . defaultCsrfMiddleware
    -- For details, see the CSRF documentation in the Yesod.Core.Handler module of the yesod-core package.
    yesodMiddleware :: ToTypedContent res => Handler res -> Handler res
    yesodMiddleware = defaultYesodMiddleware

    -- What messages should be logged. The following includes all messages when
    -- in development, and warnings and errors in production.
    shouldLogIO :: App -> LogSource -> LogLevel -> IO Bool
    shouldLogIO app _source level =
        return $
        appShouldLogAll (appSettings app)
            || level == LevelWarn
            || level == LevelError

    makeLogger :: App -> IO Logger
    makeLogger = return . appLogger

    -- Setting max body length for file upload and standard routes
    maximumContentLength _ (Just (FriendAvatarR _)) = Just (10 * 1024 * 1024) --10MB
    maximumContentLength _ _                        = Just (1024 * 1024) --1MB

    -- Error handlers
    errorHandler NotFound = fmap toTypedContent $ returnError notFound404 ("Undefined endpoint." :: Text)
    errorHandler (InternalError err) = fmap toTypedContent $ returnError status500 err
    errorHandler (InvalidArgs errors) = fmap toTypedContent $ returnError badRequest400 errors
    errorHandler (PermissionDenied err) = fmap toTypedContent $ returnError unauthorized401 err
    errorHandler NotAuthenticated = fmap toTypedContent $ returnError unauthorized401 ("Not authenticated." :: Text)
    errorHandler (BadMethod met) = fmap toTypedContent $ returnError methodNotAllowed405 (show met)

-- How to run database actions.
instance YesodPersist App where
    type YesodPersistBackend App = SqlBackend
    runDB :: SqlPersistT Handler a -> Handler a
    runDB action = do
        master <- getYesod
        runSqlPool action $ appConnPool master

instance YesodPersistRunner App where
    getDBRunner :: Handler (DBRunner App, Handler ())
    getDBRunner = defaultGetDBRunner appConnPool


instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

-- Useful when writing code that is re-usable outside of the Handler context.
-- An example is background jobs that send email.
-- This can also be useful for writing code that works across multiple Yesod applications.
instance HasHttpManager App where
    getHttpManager :: App -> Manager
    getHttpManager = appHttpManager

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger

getAppSettings :: IO AppSettings
getAppSettings = loadYamlSettings [configSettingsYml] [] useEnv

-- Run Redis in Handler monad
runRedis :: R.Redis a -> Handler a
runRedis action = do
    redisPool <- appRedisPool <$> getYesod
    liftIO $ R.runRedis redisPool action

-- Runs db in IO monad
runDBIO :: App -> SqlPersistT IO a -> IO a
runDBIO app action = runSqlPool action (appConnPool app)

-- Handler Magic-lib helper
getMime :: ByteString -> IO Text
getMime bs = do
    magic <- M.magicOpen [M.MagicMime]
    M.magicLoadDefault magic
    mime <- BS.useAsCStringLen bs (\cstring -> M.magicCString magic cstring)
    return $ pack mime

-- Serving files
serveFileOrNoContent :: (MonadHandler m) => Text -> Text -> m Value
serveFileOrNoContent mime filepath = do
    let pathStr = unpack filepath
    isFile <- liftIO $ doesFileExist pathStr
    if isFile
      then sendFile (T.encodeUtf8 mime) pathStr
      else returnNoContent
