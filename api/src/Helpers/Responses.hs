{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
module Helpers.Responses(
    Metadata(..)
  , returnPayload, returnError, returnEntity, returnCollection, returnNoContent, emptyOk200
)where

import Data.Aeson
import Yesod.Core
import Helpers.Pagination
import ClassyPrelude.Yesod

data Metadata = CollectionMetadata | PaginatedCollectionMetadata Pagination | EntityMetadata | ErrorMetadata | EmptyMetadata

instance ToJSON Metadata where
    toJSON CollectionMetadata                 = object ["type" .= ("collection" :: String)]
    toJSON (PaginatedCollectionMetadata pagi) = object ["type" .= ("collection" :: String), "pagination" .= pagi]
    toJSON EntityMetadata                     = object ["type" .= ("entity" :: String)]
    toJSON ErrorMetadata                      = object ["type" .= ("error" :: String)]
    toJSON EmptyMetadata                      = object ["type" .= ("none" :: String)]

jsonResponse :: (MonadHandler m, ToJSON a) => Text -> Status -> Metadata -> a -> m Value
jsonResponse f1 status metadata entity =
    let (Status code msg) = status in
    sendStatusJSON status (object
        ["status" .=  object
            ["message" .= (decodeUtf8 msg)
            ,"code" .= code]
        , f1 .= entity
        , "metadata" .= metadata
        ])

returnPayload :: (MonadHandler m, ToJSON a) => Status -> Metadata -> a -> m Value
returnPayload = jsonResponse "payload"

returnEntity :: (MonadHandler m, ToJSON a) => Status -> a -> m Value
returnEntity status = returnPayload status EntityMetadata

returnCollection :: (MonadHandler m, ToJSON a) => Status -> [a] -> m Value
returnCollection status = returnPayload status CollectionMetadata

returnError :: (MonadHandler m, ToJSON a) => Status -> a -> m Value
returnError status = jsonResponse "error" status ErrorMetadata

returnNoContent :: (MonadHandler m) => m Value
returnNoContent = returnPayload status204 EmptyMetadata $ object []

emptyOk200 :: (MonadHandler m) => m Value
emptyOk200 = returnPayload ok200 EmptyMetadata $ object []
