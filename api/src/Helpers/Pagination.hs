{-# LANGUAGE OverloadedStrings #-}
module Helpers.Pagination(
      Pagination(..)
    , getSimplePagination, getDetailedPagination
)where

import Yesod.Core
import Data.Aeson()
import Data.Text.Encoding (encodeUtf8)

import Helpers.Parsing (parseInt)

data Pagination = PaginationWithoutTotal {currentPage :: Integer, limit :: Integer, offset :: Integer}
                | PaginationWithTotal {currentPage :: Integer, limit :: Integer, offset :: Integer, totalEntries :: Integer, totalPages :: Integer}

instance ToJSON Pagination where
    toJSON PaginationWithoutTotal{currentPage=cur, limit=l, offset=o} = object
        [ "current" .= cur
        , "limit" .= l
        , "offset" .= o]
    toJSON PaginationWithTotal{currentPage=cur, limit=l, offset=o, totalEntries=t, totalPages=p} = object
        [ "current" .= cur
        , "limit" .= l
        , "offset" .= o
        , "total" .= t
        , "max_pages" .= p
        ]

-- Pagination
getSimplePagination :: (MonadHandler m) => Integer -> m Pagination
getSimplePagination lim = do
    maybePage <- lookupGetParam "p"
    let page = maybe 1 (max 1) $ maybePage >>= return . encodeUtf8 >>= parseInt >>= return . toInteger
    let off =  lim * (page - 1)
    return $ PaginationWithoutTotal {currentPage=page, limit=lim, offset=off}

getDetailedPagination :: (MonadHandler m) => Integer -> Integer -> m Pagination
getDetailedPagination lim total = do
    pagi <- getSimplePagination lim
    let pages = (total `quot` lim) + 1
    return $ PaginationWithTotal {currentPage=(currentPage pagi), limit=lim, offset=(offset pagi), totalEntries=total, totalPages=pages}
