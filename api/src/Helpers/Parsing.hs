module Helpers.Parsing(
    parseInt, toInt64, mergeObjects
)where

import Data.ByteString (ByteString)
import Data.Int (Int64)
import Data.Text.Encoding (decodeUtf8)
import Data.Attoparsec.Text

import Data.Aeson(Value(Object))
import qualified Data.HashMap.Lazy as HML

mergeObjects :: [Value] -> Value
mergeObjects = Object . HML.unions . map (\(Object x) -> x)

parseInt :: ByteString -> Maybe Int
parseInt str = case parseOnly (signed decimal) $ decodeUtf8 str of
    Right x -> Just x
    _       -> Nothing

toInt64 :: (Integral a) => a -> Int64
toInt64 = fromIntegral
