{-# LANGUAGE OverloadedStrings #-}
module Helpers.Filesystem(
    removeIfExists, wipeDir
)where

import Data.Foldable (foldrM)
import System.Directory

import System.FilePath.Posix ((</>), normalise, splitFileName)

removeIfExists :: FilePath -> IO Bool
removeIfExists p = do
    doesExist <- doesFileExist p
    if doesExist
        then removeFile p >> return True
        else return False


{-
Removes recursively everything from given directory, except nodes of filesystem tree
that lead to file contained in files list.
-}
wipeDir :: FilePath -> [FilePath] -> IO Bool
wipeDir pat files = do
    let path_ = normalise pat
    isFile <- doesFileExist path_
    isDir  <- doesDirectoryExist path_
    case (isFile, isDir) of
        (True, False) -> do
                          let (_, filename) = (splitFileName path_)
                          return $ not (filename `elem` files)
        (False, True) -> do
                          listing <- listDirectory path_
                          foldrM (\d acc -> do
                             let p = path_ </> d
                             b <- wipeDir p files
                             if b then removePathForcibly p else return ()
                             return $ b && acc
                           ) True listing
        _ -> return False
