{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Avatar(
    uploadAvatar, uploadAvatarFromUrl
) where

import ClassyPrelude.Yesod
import Database.Persist.Quasi()

import qualified Network.HTTP.Conduit as NET

import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as L
import qualified Codec.MIME.Parse as MIME
import qualified Codec.MIME.Type as MIME

import System.Directory (createDirectoryIfMissing)
import Database.Persist.Sql (fromSqlKey)

import Import

instance ToJSON (Entity Avatar) where
    toJSON (Entity aid a) = object
        [ "id" .= aid
        , "filename" .= avatarFilename a
        , "mime" .= avatarMime a
        ]

instance FromJSON Avatar where
    parseJSON (Object o) =
      Avatar  <$> o .: "filename"
             <*> o .: "path"
             <*> o .: "mime"
    parseJSON _ = mzero

uploadAvatarFromUrl :: App -> Text -> FriendId -> IO (Either Text (Entity Avatar))
uploadAvatarFromUrl app url friend_id = do
    manager <- NET.newManager defaultManagerSettings
    lbs <- runResourceT $ do
        req <- liftIO $ parseRequest (T.unpack url)
        res <- NET.http req manager
        runConduit $ responseBody res .| sinkLazy
    uploadAvatar app (L.toStrict lbs) friend_id


uploadAvatar :: App -> ByteString -> FriendId -> IO (Either Text (Entity Avatar))
uploadAvatar app file friend_id = do
    mime <- getMime file
    maybeExt <- filterExtension mime (appAvatarExtensions $ appSettings app)
    case maybeExt of
        Nothing -> return $ Left "File doesn't seem to be acceptable image."
        Just ext -> do
            let filepath = T.concat [appAvatarUploadDirectory $ appSettings app]
            let filename = T.concat [T.pack $ show $ fromSqlKey friend_id, ".", ext]

            -- This should be replaced with S3 or call to uploading service of some sort.
            createDirectoryIfMissing True (unpack filepath)
            BS.writeFile (T.unpack $ T.concat [filepath, filename]) file
            -- This is the place where we should enqueue background job for avatar processing

            avatar <- runDBIO app $ do
                (Entity aid av) <- insertEntity $ Avatar filename filepath mime
                updateWhere [FriendId ==. friend_id] [FriendAvatar =. (Just aid)]
                return $ Entity aid av

            return $ Right avatar


filterExtension :: Text -> [Text] -> IO (Maybe Text)
filterExtension mime allowed = do
    case MIME.parseContentType mime of
        Just (MIME.Type (MIME.Image ext) _) -> return $ if ext `elem` allowed then (Just ext) else Nothing
        _                                   -> return Nothing
