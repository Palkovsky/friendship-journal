{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Models.Grade where

import ClassyPrelude.Yesod
import Database.Persist.Quasi()
import qualified Data.HashMap.Strict as HM
import Data.Aeson ((.:?), withObject)


import Models.Model
import Models.GradeValue

gelocationToJSON :: Grade -> Maybe Value
gelocationToJSON g =
    case (gradeLatitude g, gradeLongitude g) of
        (Just lat, Just lon) -> Just $ object ["latitude" .= lat, "longitude" .= lon]
        _                    -> Nothing -- Don't display geolocations with missing cords

instance ToJSON (Entity Grade) where
    toJSON (Entity gid g) = object
        [ "id" .= gid
        , "friend_id" .= gradeFriend g
        , "description" .= gradeDescription g
        , "mark" .= gradeMark g
        , "timestamp" .= gradeTimestamp g
        , "geolocation" .= gelocationToJSON g
        ]

instance FromJSON Grade where
    parseJSON = withObject "grade" $ \o -> do
        gradeDescription <- o .: "description"
        gradeMark <- o .: "mark"
        gradeTimestamp <- o .:? "timestamp"
        geolocation <- o .:? "geolocation"
        let geolocationO = maybe HM.empty (\x -> x) geolocation
        gradeLatitude <- geolocationO .:? "latitude"
        gradeLongitude <- geolocationO .:? "longitude"
        gradeFriend <- o .: "friend_id"
        return Grade {..}


-- GradeAttrs represent attributes which are allowed to be set through POST and PUT parameters
data GradeAttrs = GradeAttrs { description :: Text
                             , mark :: GradeValue
                             , timestamp :: (Maybe Int)
                             , latitude :: (Maybe Double)
                             , longitude :: (Maybe Double)}

instance FromJSON GradeAttrs where
    parseJSON = withObject "gradeAttrs" $ \o -> do
        description <- o .: "description"
        mark <- o .: "mark"
        timestamp <- o .:? "timestamp"
        geolocation <- o .:? "geolocation"
        let geolocationO = maybe HM.empty (\x -> x) geolocation
        latitude <- geolocationO .:? "latitude"
        longitude <- geolocationO .:? "longitude"
        return GradeAttrs {..}

toGrade :: FriendId -> GradeAttrs -> Grade
toGrade friend_id attrs = Grade
    { gradeFriend = friend_id
    , gradeDescription = description attrs
    , gradeMark = mark attrs
    , gradeTimestamp = timestamp attrs
    , gradeLatitude = latitude attrs
    , gradeLongitude = longitude attrs
    }
