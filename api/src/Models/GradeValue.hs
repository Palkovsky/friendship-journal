{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.GradeValue where

import ClassyPrelude.Yesod
import Database.Persist.TH()

data GradeValue = VeryBad | Bad | Average | Good | VeryGood | Excellent
    deriving (Show, Read, Eq)

derivePersistField "GradeValue"

instance ToJSON GradeValue where
    toJSON VeryBad = "very_bad"
    toJSON Bad = "bad"
    toJSON Average = "average"
    toJSON Good = "good"
    toJSON VeryGood = "very_good"
    toJSON Excellent = "excellent"

instance FromJSON GradeValue where
    parseJSON "very_bad" = pure VeryBad
    parseJSON "bad" = pure Bad
    parseJSON "average" = pure Average
    parseJSON "good" = pure Good
    parseJSON "very_good" = pure VeryGood
    parseJSON "excellent" = pure Excellent
    parseJSON _  = pure Average

gradeValueToNum :: (Num a) => GradeValue -> a
gradeValueToNum VeryBad = 1
gradeValueToNum Bad = 2
gradeValueToNum Average = 3
gradeValueToNum Good = 4
gradeValueToNum VeryGood = 5
gradeValueToNum Excellent = 6
