{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Friend where

import ClassyPrelude.Yesod
import Database.Persist.Quasi()

import Models.Model

instance ToJSON (Entity Friend) where
    toJSON (Entity fid f) = object
        [ "id" .= fid
        , "name" .= friendName f
        , "lastname" .= friendLastname f
        , "visible" .= friendVisible f
        , "grade_avg" .= friendGradeAvg f
        , "provider_id" .= friendProviderId f 
        ]

instance FromJSON Friend where
    parseJSON (Object o) =
      Friend <$> o .: "name"
             <*> o .: "lastname"
             <*> o .: "grade_avg"
             <*> o .: "visible"
             <*> o .: "provider_id" -- id in provider serivce, helps to determine if friend was synced
             <*> o .: "avatar"
    parseJSON _ = mzero


--FriendAttrs let us define which attributes can be set by JSON recived in handler
data FriendAttrs = FriendAttrs Text Text Bool --name, lastname, visible

instance FromJSON FriendAttrs where
    parseJSON (Object o) = FriendAttrs <$> o.: "name" <*> o.: "lastname" <*> o.: "visible"
    parseJSON _          = mzero

toFriend ::  FriendAttrs -> Friend
toFriend (FriendAttrs name lastname visible) = Friend
    { friendName = name
    , friendLastname = lastname
    , friendVisible = visible
    , friendGradeAvg = 0
    , friendProviderId = Nothing
    , friendAvatar = Nothing
    }
