{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
module Routines.SyncFacebookFriends(
    create
) where

import Import
import Models.Friend()
import Models.Avatar (uploadAvatarFromUrl)

import qualified Data.Text as T
import qualified Data.Aeson as A
import qualified Data.ByteString.Lazy as BL
import qualified Network.HTTP.Conduit as NET

import Control.Concurrent
import Control.Monad (forever)

import Database.Persist.Sql()

create :: Int -> App -> IO ThreadId
create delay app = forkIO . forever $ do
    catchAny (perform app) (\ex -> putStrLn $ (T.pack . show) ex)
    threadDelay delay

perform :: App -> IO ()
perform app = do
    case appFriendsProviderUrl $ appSettings app of
        Just url -> fetchFriends app url
        Nothing  -> throwString "Friend service endpoint url not provided."

data FriendsProviderEntry = FriendsProviderEntry
    { providerId :: Text
    , name       :: Text
    , lastname   :: Text
    , avatarUrl  :: Maybe Text
    } deriving (Show)

data FriendsProviderList = FriendsProviderList
    { friends :: [FriendsProviderEntry]
    } deriving (Show)

instance FromJSON FriendsProviderEntry where
    parseJSON (Object o) = FriendsProviderEntry <$> o.: "id" <*> o.: "name" <*> o.: "lastname" <*> o A..:? "avatar"
    parseJSON _          = mzero

instance FromJSON FriendsProviderList where
    parseJSON (Object o) = FriendsProviderList <$> o.: "friends"
    parseJSON _          = mzero

fetchJson :: Text -> IO (BL.ByteString)
fetchJson url = do
    manager <- NET.newManager defaultManagerSettings
    lbs <- runResourceT $ do
        req <- liftIO $ parseRequest (T.unpack url)
        res <- NET.http req manager
        runConduit $ responseBody res .| sinkLazy
    return lbs

addFriend :: App -> FriendsProviderEntry -> IO ()
addFriend app fentry = do
    let pid = providerId fentry
    maybeFriend <- runDBIO app $ selectFirst [FriendProviderId ==. Just pid] []
    case maybeFriend of
        Just _   -> return ()
        Nothing  -> do
            let newFriend = Friend {friendName = (name fentry), friendLastname = (lastname fentry)
                                   , friendVisible = False, friendGradeAvg = 0
                                   , friendProviderId = Just pid, friendAvatar = Nothing}
            (Entity fid _) <- runDBIO app $ insertEntity newFriend
            case avatarUrl fentry of
                Just url -> do
                    _ <- uploadAvatarFromUrl app url fid
                    return ()
                Nothing  -> return ()
            return ()

fetchFriends :: App -> Text -> IO ()
fetchFriends app url = do
    lbs <- fetchJson url
    let
        obj :: Maybe FriendsProviderList
        obj = A.decode lbs
    case obj of
        Just o -> do
            mapM_ (addFriend app) (friends o)
            return ()
        Nothing -> throwString "Unable to parse response."
