{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
module Routines.DeleteUnusedAvatars(
    create
) where

import Import

import Control.Concurrent
import Control.Monad (forever)
import Data.Foldable (foldrM)
import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Helpers.Filesystem (removeIfExists)

create :: Int -> App -> IO ThreadId
create delay app = forkIO . forever $ do
    catchAny (perform app) (\ex -> putStrLn $ (T.pack . show) ex)
    threadDelay delay

fetchAvatars :: App -> IO [(Entity Avatar, Maybe (Entity Friend))]
fetchAvatars app = runDBIO app $ E.select $ E.from $ \(avatar `E.LeftOuterJoin` friend) -> do
    E.on $
      (E.just (avatar E.^. AvatarId)) E.==. (E.joinV (friend E.?. FriendAvatar))
    return (avatar, friend)


perform :: App -> IO ()
perform app = do
    avatars <- fetchAvatars app
    mapM_
      (\pair -> do
        case pair of
          (eav, Nothing) -> cleanup app eav
          _ -> return ()
      ) avatars

cleanup :: App -> Entity Avatar -> IO ()
cleanup app (Entity aid av) = do
    _ <- removeIfExists $ T.unpack (T.concat [avatarPath av, avatarFilename av])
    runDBIO app $ delete aid
    return ()
