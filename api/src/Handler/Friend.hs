{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Handler.Friend where

import Import
import Models.Friend
import Models.Avatar()

import qualified Database.Esqueleto as E
import           Database.Esqueleto ((^.), (?.))
import           Helpers.Parsing    (mergeObjects)

getFriendsR :: Handler Value
getFriendsR = do
    total <- countFriends
    pagination <- getDetailedPagination 50 total
    friends <- fetchFriends pagination
    returnPayload ok200 (PaginatedCollectionMetadata pagination) $ map friendPayload friends

postFriendsR :: Handler Value
postFriendsR = do
    friendAttrs <- (requireJsonBody :: Handler FriendAttrs)
    friend' <- runDB $ insertEntity (toFriend friendAttrs)
    returnEntity created201 friend'

getFriendR :: FriendId -> Handler Value
getFriendR friend_id = do
    maybeFriend <- fetchFriend friend_id
    case maybeFriend of
        Just friend -> returnEntity ok200 $ friendPayload friend
        Nothing     -> returnError notFound404 $ ("Not existing entity." :: String)

putFriendR :: FriendId -> Handler Value
putFriendR friend_id = do
    maybeFriend <- runDB $ selectFirst [FriendId ==. friend_id] []
    case maybeFriend of
        Just _ -> do
            friendAttrs <- (requireJsonBody :: Handler FriendAttrs)
            let friend' = toFriend friendAttrs
            runDB $ replace friend_id friend'
            returnEntity ok200 $ Entity friend_id friend'
        Nothing -> returnError notFound404 $ ("Not existing entity." :: String)

deleteFriendR :: FriendId -> Handler Value
deleteFriendR friend_id = do
    runDB $ do
        deleteWhere ([GradeFriend ==. friend_id] :: [Filter Grade])
        -- avatars without matching friends are removed in background job
        delete friend_id
    returnNoContent

optionsFriendsR ::  Handler Value
optionsFriendsR = do
  addHeader "Access-Control-Allow-Methods" "POST, OPTIONS"
  emptyOk200

optionsFriendR :: FriendId -> Handler Value
optionsFriendR _ = do
  addHeader "Access-Control-Allow-Methods" "PUT, DELETE, OPTIONS"
  emptyOk200

-- Esqueleto queries
countFriends :: Handler Integer
countFriends = do
    c <- runDB $ E.select $ E.from $ \(_ :: E.SqlExpr (Entity Friend)) -> return (E.countRows :: E.SqlExpr(E.Value Int64))
    return $ case c of
        [(E.Value x)] -> fromIntegral x
        _             -> 0


fetchFriends :: Pagination -> Handler [(Entity Friend, Maybe (Entity Avatar))]
fetchFriends pagination = do
        runDB $ E.select $ E.from $ \(friend `E.LeftOuterJoin` avatar) -> do
            E.on $ friend ^. FriendAvatar E.==. avatar ?. AvatarId
            E.limit $ fromIntegral $ limit pagination
            E.offset $ fromIntegral $ offset pagination
            return (friend, avatar)

fetchFriend :: FriendId -> Handler (Maybe (Entity Friend, Maybe (Entity Avatar)))
fetchFriend friend_id = do
    friends <- runDB $ E.select $ E.from $ \(friend `E.LeftOuterJoin` avatar) -> do
         E.on $ friend ^. FriendAvatar E.==. avatar ?. AvatarId
         E.where_ (friend ^. FriendId E.==. E.val friend_id)
         E.limit 1
         return (friend, avatar)
    return $ case friends of
        [friend] -> Just friend
        _        -> Nothing

friendPayload :: (Entity Friend, Maybe (Entity Avatar)) -> Value
friendPayload (friend, maybeAvatar) = mergeObjects [toJSON friend, object ["avatar" .= maybeAvatar]]
