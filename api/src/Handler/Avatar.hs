{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Avatar where

import qualified Data.Text.Encoding as T
import qualified Data.ByteString.Base64 as BS

import Database.Persist.Sql (toSqlKey)

import Import
import Models.Friend()
import Models.Avatar (uploadAvatar)

-- Datatype for parsin b64 encoded image
data AvatarRequest = AvatarRequest {
        getAvatar :: Text
} deriving (Show, Eq, Ord)

instance FromJSON AvatarRequest where
    parseJSON (Object v) = AvatarRequest <$> v .: "avatar"
    parseJSON _ = empty

putFriendAvatarR :: FriendId -> Handler Value
putFriendAvatarR friend_id = do
    maybeFriend <- runDB $ selectFirst [FriendId ==. friend_id] []
    case maybeFriend of
        Nothing     -> returnError notFound404 ("Not existing entity." :: String)
        Just friend -> do
            body <- (requireJsonBody :: Handler AvatarRequest)
            let fileData = BS.decode $ (T.encodeUtf8 $ getAvatar body)
            case fileData of
                Left err -> returnError badRequest400 err
                Right f  -> handleAvatarUpload friend f

handleAvatarUpload :: Entity Friend -> ByteString -> Handler Value
handleAvatarUpload (Entity friend_id _) file = do
    app <- getYesod
    eitherAvatar <- liftIO $ uploadAvatar app file friend_id
    case eitherAvatar of
        Left err     -> returnError badRequest400 err
        Right avatar -> returnEntity ok200 avatar

-- todo: Use joins here
getFriendAvatarR :: FriendId -> Handler Value
getFriendAvatarR friend_id = do
    maybeFriend <- runDB $ selectFirst [FriendId ==. friend_id] []
    let mAvatar_id = maybe Nothing (\(Entity _ fr) -> friendAvatar fr) maybeFriend
    let avatar_id  = maybe (toSqlKey 0) (\x -> x) mAvatar_id
    maybeAvatar <- runDB $ selectFirst [AvatarId ==. avatar_id] []

    case maybeAvatar of
        Just (Entity _ avatar) -> serveFileOrNoContent (avatarMime avatar) $ concat [avatarPath avatar, avatarFilename avatar]
        Nothing                -> returnNoContent

optionsFriendAvatarR :: FriendId -> Handler Value
optionsFriendAvatarR _ = do
  addHeader "Access-Control-Allow-Methods" "PUT, OPTIONS"
  emptyOk200
