{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Grade where

import Import
import Models.Grade

import Database.Persist.Sql (fromSqlKey)
import qualified Data.Text as T
import qualified Jobs.CalculateGrade as CalculateGradeJ

getGradesR :: FriendId -> Handler Value
getGradesR friend_id = do
  maybeFriend <- runDB $ selectFirst [FriendId ==. friend_id] []
  case maybeFriend of
      Just _  -> do
          grades <- runDB $ selectList [GradeFriend ==. friend_id] [Asc GradeId]
          returnCollection ok200 grades
      Nothing -> returnError notFound404 $ ("Not existing entity." :: String)

postGradesR :: FriendId -> Handler Value
postGradesR friend_id = do
   maybeFriend <- runDB $ selectFirst [FriendId ==. friend_id] []
   case maybeFriend of
       (Just _) ->  do
           inserted <- runDB . insertEntity . toGrade friend_id =<< (requireJsonBody :: Handler GradeAttrs)
           enqueueCalculateGradeJob friend_id
           returnEntity created201 inserted
       Nothing  -> returnError notFound404 $ ("Not existing entity." :: String)

putGradeR :: FriendId -> GradeId -> Handler Value
putGradeR friend_id grade_id = do
  maybeFriend <- runDB $ selectFirst [FriendId ==. friend_id] []
  maybeGrade <- runDB $ selectFirst [GradeId ==. grade_id, GradeFriend ==. friend_id] []
  case (maybeFriend, maybeGrade) of
      (Just _, Just _) -> do
          attrs <- (requireJsonBody :: Handler GradeAttrs)
          let grade' = toGrade friend_id attrs
          runDB $ replace grade_id grade'
          enqueueCalculateGradeJob friend_id
          returnEntity ok200 $ Entity grade_id grade'
      _               -> returnError notFound404 $ ("Not existing entity." :: String)


deleteGradeR :: FriendId -> GradeId -> Handler Value
deleteGradeR friend_id grade_id = do
  runDB $ do
      deleteWhere [GradeFriend ==. friend_id, GradeId ==. grade_id]
  enqueueCalculateGradeJob friend_id
  returnNoContent

optionsGradesR :: FriendId -> Handler Value
optionsGradesR _ = do
  addHeader "Access-Control-Allow-Methods" "POST, OPTIONS"
  emptyOk200

optionsGradeR :: FriendId -> GradeId -> Handler Value
optionsGradeR _ _ = do
  addHeader "Access-Control-Allow-Methods" "PUT, DELETE, OPTIONS"
  emptyOk200


enqueueCalculateGradeJob :: FriendId -> Handler ()
enqueueCalculateGradeJob friend_id = do
    app <- getYesod
    let key = encodeUtf8 . T.pack . show $ fromSqlKey friend_id
    _ <- liftIO $ CalculateGradeJ.create app key
    return ()
